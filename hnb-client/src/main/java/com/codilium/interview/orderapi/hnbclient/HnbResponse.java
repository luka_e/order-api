package com.codilium.interview.orderapi.hnbclient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HnbResponse {
    String broj_tecajnice;
    String datum_primjene;
    String drzava;
    String drzava_iso;
    String sifra_valute;
    String valuta;
    Integer jedinica;
    String kupovni_tecaj;
    String srednji_tecaj;
    String prodajni_tecaj;
}
