package com.codilium.interview.orderapi.hnbclient;

import com.codilium.interview.domain.api.exception.ServerException;
import com.codilium.interview.domain.spi.ExchangeRepository;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

@RequiredArgsConstructor
public class HnbExchangeRepository implements ExchangeRepository {

    private final String url;

    @Override
    public BigDecimal getExchangeRate() throws ServerException {
        val restTemplate = new RestTemplate();
        try {
            val result = restTemplate.getForObject(url, HnbResponse[].class);
            if (result == null || result.length == 0) {
                return null;
            }
            val exchangeRate = Double.parseDouble(result[0].srednji_tecaj.replace(",", "."));
            return BigDecimal.valueOf(exchangeRate);
        } catch (Exception e) {
            throw new ServerException();
        }
    }
}
