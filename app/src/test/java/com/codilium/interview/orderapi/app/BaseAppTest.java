package com.codilium.interview.orderapi.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
@SpringBootTest
@ActiveProfiles("test")
abstract public class BaseAppTest<T, R> {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    protected abstract String getBasePath();
    protected abstract Class<T> getBaseType();
    protected abstract R getRequestExample();

    @Test
    public void whenGetInexistentId_thenReturn404() throws Exception {
        get(12356, 404);
    }

    @Test
    public void whenUpdateInexistentId_thenReturn404() throws Exception {
        update(getRequestExample(), 12356, 404);
    }

    @Test
    public void whenDeleteInexistentId_thenReturn404() throws Exception {
        delete(12356, 404);
    }

    protected T create(R request, int expectedStatus) throws Exception {
        var response = mockMvc.perform(post(getBasePath())
                .content(objectMapper.writeValueAsString(request))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();
        assertEquals(expectedStatus, response.getStatus());
        if(expectedStatus != 200) {
            return null;
        }
        return objectMapper.readValue(response.getContentAsByteArray(), getBaseType());
    }

    protected T update(R request, long id, int expectedStatus) throws Exception {
        var response = mockMvc.perform(put(getBasePath() + "/" + id)
                .content(objectMapper.writeValueAsString(request))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();
        assertEquals(expectedStatus, response.getStatus());
        if(expectedStatus != 200) {
            return null;
        }
        return objectMapper.readValue(response.getContentAsByteArray(), getBaseType());
    }

    protected T delete(long id, int expectedStatus) throws Exception {
        var response = mockMvc.perform(MockMvcRequestBuilders.delete(getBasePath() + "/" + id))
                .andReturn()
                .getResponse();
        assertEquals(expectedStatus, response.getStatus());
        if(expectedStatus != 200) {
            return null;
        }
        return objectMapper.readValue(response.getContentAsByteArray(), getBaseType());
    }

    protected T get(long id, int expectedStatus) throws Exception {
        var response = mockMvc.perform(MockMvcRequestBuilders.get(getBasePath() + "/" + id))
                .andReturn()
                .getResponse();
        assertEquals(expectedStatus, response.getStatus());
        if(expectedStatus != 200) {
            return null;
        }
        return objectMapper.readValue(response.getContentAsByteArray(), getBaseType());
    }
}
