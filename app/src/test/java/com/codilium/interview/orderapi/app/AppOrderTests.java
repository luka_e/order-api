package com.codilium.interview.orderapi.app;

import com.codilium.interview.domain.spi.CustomerRepository;
import com.codilium.interview.domain.spi.ProductRepository;
import com.codilium.interview.orderapi.domain.model.Order;
import com.codilium.interview.orderapi.domain.model.OrderStatus;
import com.codilium.interview.orderapi.hnbclient.HnbResponse;
import com.codilium.interview.orderapi.rest.order.model.OrderItemRequest;
import com.codilium.interview.orderapi.rest.order.model.OrderRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class AppOrderTests extends BaseAppTest<Order, OrderRequest>{

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProductRepository productRepository;

    private static WireMockServer wireMockServer;

    @Override
    protected String getBasePath() {
        return "/order";
    }

    @Override
    protected Class<Order> getBaseType() {
        return Order.class;
    }

    @Override
    protected OrderRequest getRequestExample() {
        return buildOrderRequest();
    }

    @BeforeAll
    public static void setupWiremock() {
        wireMockServer = new WireMockServer(options().port(8888));
        WireMock.configureFor("localhost", 8888);
        wireMockServer.start();
    }

    @BeforeEach
    public void setupMockResponse() throws JsonProcessingException {
        wireMockServer.stubFor(WireMock.get(urlEqualTo("/tecajn/v2?valuta=EUR"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody(new ObjectMapper().writeValueAsString(getMockResponse()))
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)));
    }

    @AfterAll
    public static void stopWiremock() {
        wireMockServer.stop();
    }

    @Test
    public void whenOrderCreateIsCalled_thenItReturnsTheCreatedOrder() throws Exception {
        var orderRequest = buildOrderRequest();

        var order = create(orderRequest, 200);
        assertEquals(0, order.getTotalPriceHrk().compareTo(BigDecimal.valueOf(1000.00)));
        assertEquals(1, order.getOrderItems().size());
        assertEquals(customerRepository.findById(1), order.getCustomer());
        assertEquals(1L, (long)order.getOrderItems().get(0).getProduct().getId());
        assertEquals(productRepository.findById(1), order.getOrderItems().get(0).getProduct());
    }

    @Test
    public void whenOrderCreateIsCalled_thenItCanBeFetched() throws Exception {
        var orderRequest = buildOrderRequest();
        var createdOrder = create(orderRequest, 200);
        var fetchedOrder = get(createdOrder.getId(), 200);
        assertEquals(createdOrder, fetchedOrder);
    }

    @Test
    public void whenOrderUpdateIsCalled_thenItIsReturnsUpdatedObject() throws Exception {
        var orderRequest = buildOrderRequest();
        var createdOrder = create(orderRequest, 200);
        orderRequest.setOrderItems(List.of(new OrderItemRequest(2L, BigDecimal.valueOf(2))));
        var updatedOrder = update(orderRequest, createdOrder.getId(), 200);
        assertEquals(0, updatedOrder.getTotalPriceHrk().compareTo(BigDecimal.valueOf(400.00)));
        assertEquals(1, updatedOrder.getOrderItems().size());
        assertEquals(2L, (long)updatedOrder.getOrderItems().get(0).getProduct().getId());
        assertEquals(productRepository.findById(2), updatedOrder.getOrderItems().get(0).getProduct());
    }

    @Test
    public void whenDeleteIsCalled_thenItReturnsDeletedObject() throws Exception {
        var orderRequest = buildOrderRequest();
        var createdOrder = create(orderRequest, 200);
        var deletedOrder = delete(createdOrder.getId(), 200);
        assertEquals(0, deletedOrder.getTotalPriceHrk().compareTo(BigDecimal.valueOf(1000.00)));
        assertEquals(1, deletedOrder.getOrderItems().size());
        assertEquals(1L, (long)deletedOrder.getOrderItems().get(0).getProduct().getId());
        assertEquals(productRepository.findById(1), deletedOrder.getOrderItems().get(0).getProduct());
    }

    @Test
    public void whenOrderCreateIsCalledWithInexistentProduct_thenItReturns400() throws Exception {
        var orderRequest = buildOrderRequest();
        orderRequest.setOrderItems(List.of(new OrderItemRequest(4321L, BigDecimal.TEN)));
        create(orderRequest, 400);
    }

    @Test
    public void whenOrderCreateIsCalledWithUnavailableProduct_thenItReturns400() throws Exception {
        var orderRequest = buildOrderRequest();
        orderRequest.setOrderItems(List.of(new OrderItemRequest(4L, BigDecimal.TEN)));
        create(orderRequest, 400);
    }

    @Test
    public void whenOrderUpdateIsCalledWithInexistentProduct_thenItReturns400() throws Exception {
        var orderRequest = buildOrderRequest();
        var createdOrder = create(orderRequest, 200);
        orderRequest.setOrderItems(List.of(new OrderItemRequest(4321L, BigDecimal.TEN)));
        update(orderRequest, createdOrder.getId(), 400);
    }

    @Test
    public void whenOrderUpdateIsCalledWithUnavailableProduct_thenItReturns400() throws Exception {
        var orderRequest = buildOrderRequest();
        var createdOrder = create(orderRequest, 200);
        orderRequest.setOrderItems(List.of(new OrderItemRequest(4L, BigDecimal.TEN)));
        update(orderRequest, createdOrder.getId(), 400);
    }

    @Test
    public void whenDeleteIsCalled_thenGetReturns404() throws Exception {
        var orderRequest = buildOrderRequest();
        var createdOrder = create(orderRequest, 200);
        var deletedOrder = delete(createdOrder.getId(), 200);
        get(deletedOrder.getId(), 404);
    }

    @Test
    public void whenSubmitIsCalled_thenOrderGetsDateAndCorrectStatus() throws Exception {
        var orderRequest = buildOrderRequest();
        var createdOrder = create(orderRequest, 200);
        var submitedOrder = submitOrder(createdOrder.getId(), 200);
        assertNotNull(submitedOrder.getTotalPriceEur());
        assertNotNull(submitedOrder.getSubmittedAt());
        assertEquals(OrderStatus.SUBMITTED, submitedOrder.getStatus());
    }

    @Test
    public void whenSubmitIsCalled_thenOrderCannotBeDeleted() throws Exception {
        var orderRequest = buildOrderRequest();
        var createdOrder = create(orderRequest, 200);
        submitOrder(createdOrder.getId(), 200);
        delete(createdOrder.getId(), 400);
    }

    @Test
    public void whenSubmitIsCalled_thenOrderCannotBeUpdated() throws Exception {
        var orderRequest = buildOrderRequest();
        var createdOrder = create(orderRequest, 200);
        var submitedOrder = submitOrder(createdOrder.getId(), 200);
        orderRequest.setOrderItems(List.of(new OrderItemRequest(3L, BigDecimal.valueOf(5))));
        update(orderRequest, submitedOrder.getId(), 400);
    }

    @Test
    public void whenSubmitIsCalledTwice_thenOrderCannotBeSubmittedSecondTime() throws Exception {
        var orderRequest = buildOrderRequest();
        var createdOrder = create(orderRequest, 200);
        var submitedOrder = submitOrder(createdOrder.getId(), 200);
        submitOrder(submitedOrder.getId(), 400);
    }

    @Test
    public void whenSubmitIsCalled_thenValueFromExchangeServiceIsUsed() throws Exception {
        var orderRequest = buildOrderRequest();
        var createdOrder = create(orderRequest, 200);
        var submitedOrder = submitOrder(createdOrder.getId(), 200);
        var calculatedPrice = submitedOrder.getTotalPriceHrk().divide(BigDecimal.valueOf(7.55), 2, RoundingMode.HALF_UP);
        assertEquals(calculatedPrice, submitedOrder.getTotalPriceEur());
    }

    @Test
    public void whenExchangeServerCallFails_thenReturns500() throws Exception {
        wireMockServer.stubFor(WireMock.get(urlEqualTo("/tecajn/v2?valuta=EUR"))
                .willReturn(aResponse()
                        .withStatus(404)
                        .withBody("\"error\":\"not found\"")
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)));
        var orderRequest = buildOrderRequest();
        var createdOrder = create(orderRequest, 200);
        submitOrder(createdOrder.getId(), 500);
    }

    protected Order submitOrder(long id, int expectedStatus) throws Exception {
        var response = mockMvc.perform(patch(getBasePath() + "/" + id + "/submit"))
                .andReturn()
                .getResponse();
        assertEquals(expectedStatus, response.getStatus());
        if(expectedStatus != 200) {
            return null;
        }
        return objectMapper.readValue(response.getContentAsByteArray(), getBaseType());
    }

    private OrderRequest buildOrderRequest() {
        var orderRequest = new OrderRequest();
        orderRequest.setCustomerId(1L);
        var orderItems = List.of(
                new OrderItemRequest(1L, BigDecimal.TEN));
        orderRequest.setOrderItems(orderItems);
        return orderRequest;
    }

    private static List<HnbResponse> getMockResponse() {
        return List.of(new HnbResponse(
                "84",
                "2021-05-03",
                "EMU",
                "EMU",
                "978",
                "EUR",
                1,
                "7,520000",
                "7,550000",
                "7,570000"));
    }
}
