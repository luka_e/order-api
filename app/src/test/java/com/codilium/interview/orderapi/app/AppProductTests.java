package com.codilium.interview.orderapi.app;

import com.codilium.interview.domain.spi.ProductRepository;
import com.codilium.interview.orderapi.domain.model.Product;
import com.codilium.interview.orderapi.rest.product.model.ProductRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class AppProductTests extends BaseAppTest<Product, ProductRequest>{

    @Override
    protected String getBasePath() {
        return "/product";
    }

    @Override
    protected Class<Product> getBaseType() {
        return Product.class;
    }

    @Override
    protected ProductRequest getRequestExample() {
        return new ProductRequest("test product", "1234543210", BigDecimal.ONE, "", true);
    }

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void whenRequestProductList_thenReturnTestData() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/product")).andReturn();
        Product[] parsedResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Product[].class);
        var productsFromDb = productRepository.getProducts(1, 10);
        assertEquals(4, parsedResponse.length);
        assertEquals(productsFromDb, Arrays.asList(parsedResponse));
    }

    @Test
    public void whenRequestProductListWithParams_thenReturnCorrectSizeList() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/product")
                .param("page", "1")
                .param("limit", "2"))
                .andReturn();
        Product[] parsedResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Product[].class);
        assertEquals(2, parsedResponse.length);
    }

    @Test
    public void whenRequestProductListWithParamsOutOfRange_thenReturnEmptyList() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/product")
                        .param("page", "2")
                        .param("limit", "10"))
                .andReturn();
        Product[] parsedResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Product[].class);
        assertEquals(0, parsedResponse.length);
    }

    @Test
    public void whenRequestSpecificItem_thenItIsCorrectlyReturned() throws Exception {
        var productFromDb = productRepository.findById(1);
        Product parsedResponse = get(1, 200);
        assertEquals(productFromDb, parsedResponse);
    }

    @Test
    public void whenItemIsCreated_thenResponseCodeIs200() throws Exception {
        ProductRequest product = new ProductRequest("test product", "1234543210", BigDecimal.ONE, "", true);
        var createdProduct = create(product, 200);
        productRepository.delete(createdProduct.getId());
    }

    @Test
    public void whenItemIsCreated_thenItReturnsObjectWithIdAndIsInDb() throws Exception {
        ProductRequest product = new ProductRequest("test product", "1234543211", BigDecimal.ONE, "", true);
        Product cratedProduct = create(product, 200);
        var productFromDb = productRepository.findById(cratedProduct.getId());
        assertEquals(productFromDb, cratedProduct);
        productRepository.delete(cratedProduct.getId());
    }

    @Test
    public void whenPerformUpdateAndProductCodeIsTooLong_thenItReturns400() throws Exception {
        ProductRequest productRequest = new ProductRequest("test product", "12345432111", BigDecimal.ONE, "", true);
        update(productRequest, 1, 400);
    }

    @Test
    public void whenPerformUpdateAndProductCodeIsTooShort_thenItReturns400() throws Exception {
        ProductRequest productRequest = new ProductRequest("test product", "12345432", BigDecimal.ONE, "", true);
        update(productRequest, 1, 400);
    }

    @Test
    public void whenPerformUpdateAndProductCodeIsMissing_thenItReturns400() throws Exception {
        ProductRequest productRequest = new ProductRequest("test product", null, BigDecimal.ONE, "", true);
        update(productRequest, 1, 400);
    }

    @Test
    public void whenPerformUpdateAndPriceIsLessThanZero_thenItReturns400() throws Exception {
        ProductRequest productRequest = new ProductRequest("test product", "1234543211", BigDecimal.valueOf(-1), "", true);
        update(productRequest, 1, 400);
    }

    @Test
    public void whenPerformUpdateAndAvailabilityIsMissing_thenItReturns400() throws Exception {
        ProductRequest productRequest = new ProductRequest("test product", "1234543211", BigDecimal.ONE, "", null);
        update(productRequest, 1, 400);
    }

    @Test
    public void whenPerformUpdateAndNameIsMissing_thenItReturns400() throws Exception {
        ProductRequest productRequest = new ProductRequest(null, "1234543211", BigDecimal.ONE, "", true);
        update(productRequest, 1, 400);
    }

    @Test
    public void whenPerformUpdate_thenItReturns200AndUpdatedProduct() throws Exception {
        ProductRequest productRequest = new ProductRequest("test product", "1234543211", BigDecimal.ONE, "", true);
        var createdProduct = create(productRequest, 200);
        productRequest.setName("updated name");
        var updatedProduct = update(productRequest, createdProduct.getId(), 200);
        assertEquals("updated name", updatedProduct.getName());
        productRepository.delete(createdProduct.getId());
    }

    @Test
    public void whenPerformDelete_thenItReturns200AndDeletedProduct() throws Exception {
        var product = create(getRequestExample(), 200);
        var deletedProduct = delete(product.getId(), 200);
        assertEquals(product.getId(), deletedProduct.getId());
    }
}
