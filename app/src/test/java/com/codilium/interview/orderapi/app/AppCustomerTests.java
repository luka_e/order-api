package com.codilium.interview.orderapi.app;

import com.codilium.interview.domain.spi.CustomerRepository;
import com.codilium.interview.orderapi.domain.model.Customer;
import com.codilium.interview.orderapi.rest.customer.model.CustomerRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AppCustomerTests extends BaseAppTest<Customer, CustomerRequest> {

    @Autowired
    protected CustomerRepository customerRepository;

    @Override
    protected String getBasePath() {
        return "/customer";
    }

    @Override
    protected Class<Customer> getBaseType() {
        return Customer.class;
    }

    @Override
    protected CustomerRequest getRequestExample() {
        return new CustomerRequest("first name", "last name", "firstname.lastname@test.com");
    }

    @Test
    public void whenRequestCustomerList_thenReturnTestData() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/customer")).andReturn();
        Customer[] parsedResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Customer[].class);
        var productsFromDb = customerRepository.findAll(1, 10);
        assertEquals(3, parsedResponse.length);
        assertEquals(productsFromDb, Arrays.asList(parsedResponse));
    }

    @Test
    public void whenRequestCustomerListWithParams_thenReturnCorrectSizeList() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/customer")
                        .param("page", "1")
                        .param("limit", "2"))
                .andReturn();
        Customer[] parsedResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Customer[].class);
        assertEquals(2, parsedResponse.length);
    }

    @Test
    public void whenRequestCustomerListWithParamsOutOfRange_thenReturnEmptyList() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/customer")
                        .param("page", "2")
                        .param("limit", "10"))
                .andReturn();
        Customer[] parsedResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Customer[].class);
        assertEquals(0, parsedResponse.length);
    }

    @Test
    public void whenCreateCustomerWithNullFirstName_thenReturn400() throws Exception {
        final var customerRequest = getRequestExample();
        customerRequest.setFirstName(null);
        create(customerRequest, 400);
    }

    @Test
    public void whenCreateCustomerWithNullLastName_thenReturn400() throws Exception {
        final var customerRequest = getRequestExample();
        customerRequest.setLastName(null);
        create(customerRequest, 400);
    }

    @Test
    public void whenCreateCustomerWithBlankFirstName_thenReturn400() throws Exception {
        final var customerRequest = getRequestExample();
        customerRequest.setFirstName("");
        create(customerRequest, 400);
    }

    @Test
    public void whenCreateCustomerWithBlankLastName_thenReturn400() throws Exception {
        final var customerRequest = getRequestExample();
        customerRequest.setLastName("");
        create(customerRequest, 400);
    }

    @Test
    public void whenCreateCustomerWithNullEmail_thenReturn400() throws Exception {
        final var customerRequest = getRequestExample();
        customerRequest.setEmail(null);
        create(customerRequest, 400);
    }

    @Test
    public void whenCreateCustomerWithBlankEmail_thenReturn400() throws Exception {
        final var customerRequest = getRequestExample();
        customerRequest.setEmail("");
        create(customerRequest, 400);
    }

    @Test
    public void whenCreateCustomerWithMallformedEmail_thenReturn400() throws Exception {
        final var customerRequest = getRequestExample();
        getMalformedEmails().forEach(malformedEmail -> {
            customerRequest.setEmail(malformedEmail);
            try {
                create(customerRequest, 400);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void whenUpdateCustomerWithNullFirstName_thenReturn400() throws Exception {
        final var customerRequest = getRequestExample();
        customerRequest.setFirstName(null);
        update(customerRequest, 1, 400);
    }

    @Test
    public void whenUpdateCustomerWithNullLastName_thenReturn400() throws Exception {
        final var customerRequest = getRequestExample();
        customerRequest.setLastName(null);
        update(customerRequest, 1,400);
    }

    @Test
    public void whenUpdateCustomerWithBlankFirstName_thenReturn400() throws Exception {
        final var customerRequest = getRequestExample();
        customerRequest.setFirstName("");
        update(customerRequest, 1,400);
    }

    @Test
    public void whenUpdateCustomerWithBlankLastName_thenReturn400() throws Exception {
        final var customerRequest = getRequestExample();
        customerRequest.setLastName("");
        update(customerRequest, 1,400);
    }

    @Test
    public void whenUpdateCustomerWithNullEmail_thenReturn400() throws Exception {
        final var customerRequest = getRequestExample();
        customerRequest.setEmail(null);
        update(customerRequest, 1,400);
    }

    @Test
    public void whenUpdateCustomerWithBlankEmail_thenReturn400() throws Exception {
        final var customerRequest = getRequestExample();
        customerRequest.setEmail("");
        update(customerRequest, 1,400);
    }

    @Test
    public void whenUpdateCustomerWithMallformedEmail_thenReturn400() throws Exception {
        final var customerRequest = getRequestExample();
        getMalformedEmails().forEach(malformedEmail -> {
            customerRequest.setEmail(malformedEmail);
            try {
                update(customerRequest, 1,400);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    private List<String> getMalformedEmails() {
        return List.of(
                "test",
                "test.test@.com",
                "@test.com"
        );
    }
}
