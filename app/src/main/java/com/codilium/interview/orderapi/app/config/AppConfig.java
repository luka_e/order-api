package com.codilium.interview.orderapi.app.config;

import com.codilium.interview.domain.api.CustomerService;
import com.codilium.interview.domain.api.OrderService;
import com.codilium.interview.domain.api.ProductService;
import com.codilium.interview.domain.spi.CustomerRepository;
import com.codilium.interview.domain.spi.ExchangeRepository;
import com.codilium.interview.domain.spi.OrderRepository;
import com.codilium.interview.domain.spi.ProductRepository;
import com.codilium.interview.orderapi.DefaultCustomerService;
import com.codilium.interview.orderapi.DefaultOrderService;
import com.codilium.interview.orderapi.DefaultProductService;
import com.codilium.interview.orderapi.hnbclient.HnbExchangeRepository;
import com.codilium.interview.orderapi.repository.config.RepositoryConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(RepositoryConfig.class)
@EnableAutoConfiguration
public class AppConfig {

    @Value( "${exchange.api.url}" )
    private String exchangeApiUrl;

    @Bean
    public ProductService provideProductService(ProductRepository productRepository) {
        return new DefaultProductService(productRepository);
    }

    @Bean
    public CustomerService provideCustomerService(CustomerRepository customerRepository) {
        return new DefaultCustomerService(customerRepository);
    }

    @Bean
    public OrderService provideOrderService(
            OrderRepository orderRepository,
            CustomerRepository customerRepository,
            ProductRepository productRepository,
            ExchangeRepository exchangeRepository
    ) {
        return new DefaultOrderService(orderRepository, customerRepository, productRepository, exchangeRepository);
    }

    @Bean
    public ExchangeRepository provideExchangeRepository() {
        return new HnbExchangeRepository(exchangeApiUrl);
    }
}
