package com.codilium.interview.orderapi.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.codilium.interview.orderapi.repository.*")
@EntityScan("com.codilium.interview.orderapi.repository.entity")
@ComponentScan({"com.codilium.interview.orderapi.*","com.codilium.interview.orderapi.rest.*"})
public class AppApplication {
	public static void main(String[] args) {
		SpringApplication.run(AppApplication.class, args);
	}
}
