package com.codilium.interview.orderapi;

import com.codilium.interview.domain.api.CustomerService;
import com.codilium.interview.domain.spi.CustomerRepository;
import com.codilium.interview.orderapi.domain.model.Customer;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class DefaultCustomerService implements CustomerService {

    private final CustomerRepository customerRepository;

    @Override
    public Customer addCustomer(String firstName, String lastName, String email) {
        return customerRepository.create(new Customer(firstName, lastName, email));
    }

    @Override
    public Customer updateCustomer(long id, String firstName, String lastName, String email) {
        customerRepository.findById(id);
        return customerRepository.update(new Customer(id, firstName, lastName, email));
    }

    @Override
    public Customer deleteCustomer(long id) {
        return customerRepository.delete(id);
    }

    @Override
    public Customer getCustomer(long id) {
        return customerRepository.findById(id);
    }

    @Override
    public List<Customer> getCustomers(Integer page, Integer limit) {
        if (page == null || page == 0) {
            page = 1;
        }
        if (limit == null || limit == 0 || limit > 100) {
            limit = 20;
        }
        return customerRepository.findAll(page, limit);
    }
}
