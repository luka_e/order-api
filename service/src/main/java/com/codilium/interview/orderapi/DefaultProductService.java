package com.codilium.interview.orderapi;

import com.codilium.interview.domain.api.ProductService;
import com.codilium.interview.domain.spi.ProductRepository;
import com.codilium.interview.orderapi.domain.model.Product;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@RequiredArgsConstructor
public class DefaultProductService implements ProductService {

    private final ProductRepository productRepository;

    public Product addProduct(String name, String code, BigDecimal price, String description, Boolean isAvailable) {
        return productRepository.create(new Product(name, code, price, description, isAvailable));
    }

    public Product updateProduct(Long id, String name, String code, BigDecimal price, String description, Boolean isAvailable) {
        productRepository.findById(id);
        return productRepository.update(new Product(id, name, code, price, description, isAvailable));
    }

    public Product deleteProduct(Long id) {
        return productRepository.delete(id);
    }

    public Product getProduct(Long id) {
        return productRepository.findById(id);
    }

    public List<Product> getProducts(Integer page, Integer limit) {
        if (page == null || page == 0) {
            page = 1;
        }
        if (limit == null || limit == 0 || limit > 100) {
            limit = 20;
        }
        return productRepository.getProducts(page, limit);
    }
}
