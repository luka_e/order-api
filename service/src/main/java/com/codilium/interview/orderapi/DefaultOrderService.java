package com.codilium.interview.orderapi;

import com.codilium.interview.domain.api.OrderService;
import com.codilium.interview.domain.api.exception.BadRequestException;
import com.codilium.interview.domain.api.exception.ServerException;
import com.codilium.interview.domain.spi.CustomerRepository;
import com.codilium.interview.domain.spi.ExchangeRepository;
import com.codilium.interview.domain.spi.OrderRepository;
import com.codilium.interview.domain.spi.ProductRepository;
import com.codilium.interview.orderapi.domain.model.Order;
import com.codilium.interview.orderapi.domain.model.OrderItem;
import com.codilium.interview.orderapi.domain.model.OrderStatus;
import com.codilium.interview.orderapi.domain.model.Product;
import lombok.RequiredArgsConstructor;
import lombok.val;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class DefaultOrderService implements OrderService {

    private static final String ORDER_ALREADY_SUBMITTED = "Order already submitted";
    private static final String ORDER_CONTAINS_INEXISTANT_PRODUCTS = "Order request containts inexistent products";
    private static final String ORDER_CONTAINS_UNAVAILABLE_PRODUCTS = "Order request containts unavailable products";

    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;
    private final ProductRepository productRepository;
    private final ExchangeRepository exchangeRepository;

    @Override
    public Order getOrder(long id) {
        return this.orderRepository.findOrderById(id);
    }

    @Override
    public Order addOrder(long customerId, List<OrderItem> orderItems) throws BadRequestException {
        val customer = this.customerRepository.findById(customerId);
        val products = getProductsByOrderItems(orderItems);
        if(orderItems.size() > products.size()) {
            throw new BadRequestException(ORDER_CONTAINS_INEXISTANT_PRODUCTS);
        }
        if(hasUnavailableProducts(products)) {
            throw new BadRequestException(ORDER_CONTAINS_UNAVAILABLE_PRODUCTS);
        }
        val totalPrice = calculateSum(products, orderItems);
        val order = new Order(customer, orderItems, totalPrice);
        val createdOrder = orderRepository.create(order);
        remapProductsInOrder(createdOrder, products);
        return createdOrder;
    }

    @Override
    public Order updateOrder(long orderId, long customerId, final List<OrderItem> orderItems) throws BadRequestException {
        val existingOrder = getOrder(orderId);
        if(existingOrder.isSubmitted()) {
            throw new BadRequestException(ORDER_ALREADY_SUBMITTED);
        }
        val products = getProductsByOrderItems(orderItems);
        if(orderItems.size() > products.size()) {
            throw new BadRequestException(ORDER_CONTAINS_INEXISTANT_PRODUCTS);
        }
        if(hasUnavailableProducts(products)) {
            throw new BadRequestException(ORDER_CONTAINS_UNAVAILABLE_PRODUCTS);
        }
        val totalPrice = calculateSum(products, orderItems);
        existingOrder.setOrderItems(orderItems);
        existingOrder.setTotalPriceHrk(totalPrice);
        val updatedOrder = orderRepository.update(existingOrder);
        remapProductsInOrder(updatedOrder, products);
        return updatedOrder;
    }

    @Override
    public Order deleteOrder(long id) throws BadRequestException {
        val order = getOrder(id);
        if(order.isSubmitted()) {
            throw new BadRequestException(ORDER_ALREADY_SUBMITTED);
        }
        orderRepository.delete(id);
        return order;
    }

    @Override
    public List<Order> getOrders(Integer page, Integer limit) {
        if (page == null || page == 0) {
            page = 1;
        }
        if (limit == null || limit == 0 || limit > 100) {
            limit = 20;
        }
        return orderRepository.getOrders(page, limit);
    }

    @Override
    public Order submitOrder(long orderId) throws BadRequestException, ServerException {
        val existingOrder = getOrder(orderId);
        if(existingOrder.isSubmitted()) {
            throw new BadRequestException(ORDER_ALREADY_SUBMITTED);
        }
        val exchangeRate = exchangeRepository.getExchangeRate();
        if(exchangeRate == null) {
            throw new ServerException();
        }
        existingOrder.setStatus(OrderStatus.SUBMITTED);
        val priceEur = existingOrder.getTotalPriceHrk().divide(exchangeRate, 2, RoundingMode.HALF_UP);
        existingOrder.setTotalPriceEur(priceEur);
        existingOrder.setSubmittedAt(new Date());
        return orderRepository.update(existingOrder);
    }

    private void remapProductsInOrder(Order order, List<Product> products) {
        order.getOrderItems().forEach(orderItem ->
                orderItem.setProduct(
                        products.stream().filter(product -> product.getId().equals(orderItem.getProduct().getId())).findFirst().orElseThrow()
                ));
    }

    private List<Product> getProductsByOrderItems(List<OrderItem> orderItems) {
        return productRepository.getProducts(
                orderItems
                        .stream()
                        .map(orderItem -> orderItem.getProduct().getId())
                        .collect(Collectors.toList())
        );
    }
    private BigDecimal calculateSum(List<Product> products, List<OrderItem> orderItems) {
        var itemPrices =  orderItems.stream().map(orderItem -> {
            var price = products
                    .stream()
                    .filter(product -> product.getId().equals(orderItem.getProduct().getId()))
                    .findFirst()
                    .orElseThrow()
                    .getPrice();
            return orderItem.getQuantity().multiply(price);
        });
        return itemPrices.reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private boolean hasUnavailableProducts(List<Product> products) {
        return products.stream().anyMatch(product -> !product.getAvailable());
    }
}
