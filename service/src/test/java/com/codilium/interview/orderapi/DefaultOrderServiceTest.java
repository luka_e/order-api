package com.codilium.interview.orderapi;

import com.codilium.interview.domain.api.exception.BadRequestException;
import com.codilium.interview.domain.api.exception.ServerException;
import com.codilium.interview.domain.spi.CustomerRepository;
import com.codilium.interview.domain.spi.ExchangeRepository;
import com.codilium.interview.domain.spi.OrderRepository;
import com.codilium.interview.domain.spi.ProductRepository;
import com.codilium.interview.orderapi.domain.model.Order;
import com.codilium.interview.orderapi.domain.model.OrderItem;
import com.codilium.interview.orderapi.domain.model.OrderStatus;
import com.codilium.interview.orderapi.domain.model.Product;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class DefaultOrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ExchangeRepository exchangeRepository;

    @Mock
    private Product product;

    @Mock
    private Order order;

    private DefaultOrderService service;

    @Before
    public void setUp() throws ServerException {
        MockitoAnnotations.openMocks(this);
        this.service = new DefaultOrderService(orderRepository, customerRepository, productRepository, exchangeRepository);
        when(product.getId()).thenReturn(1L);
        when(productRepository.findById(anyLong())).thenReturn(product);
        when(productRepository.delete(anyLong())).thenReturn(product);
        when(orderRepository.findOrderById(anyLong())).thenReturn(order);
        when(exchangeRepository.getExchangeRate()).thenReturn(BigDecimal.valueOf(7.55));
        when(order.getTotalPriceHrk()).thenReturn(BigDecimal.valueOf(1000));
        when(orderRepository.update(any())).thenReturn(this.order);
    }

    @Test
    public void getOrderReturnsObjectFromRepository() {
        var order = this.service.getOrder(1);
        verify(orderRepository).findOrderById(1);
        assertEquals(this.order, order);
    }

    @Test(expected = NoSuchElementException.class)
    public void getOrderThrowsExceptionFromRepository() {
        when(orderRepository.findOrderById(404)).thenThrow(NoSuchElementException.class);
        this.service.getOrder(404);
    }

    @Test
    public void deleteOrderReturnsObjectFromRepository() throws BadRequestException {
        var order = this.service.deleteOrder(1);
        verify(orderRepository).findOrderById(1);
        verify(orderRepository).delete(1);
        assertEquals(this.order, order);
    }

    @Test(expected = NoSuchElementException.class)
    public void deleteOrderThrowsExceptionFromRepository() throws BadRequestException {
        doThrow(NoSuchElementException.class).when(orderRepository).delete(anyLong());
        this.service.deleteOrder(404);
    }

    @Test(expected = BadRequestException.class)
    public void deleteOrderThrowsBadRequestWhenSubmitted() throws BadRequestException {
        when(order.getStatus()).thenReturn(OrderStatus.SUBMITTED);
        when(order.isSubmitted()).thenReturn(true);
        this.service.deleteOrder(1);
    }

    @Test
    public void submitUpdatesOrderAndReturnsIt() throws BadRequestException, ServerException {
        var order = this.service.submitOrder(1);
        verify(exchangeRepository).getExchangeRate();
        var calculatedPrice = BigDecimal.valueOf(1000).divide(BigDecimal.valueOf(7.55), 2, RoundingMode.HALF_UP);
        verify(order).setTotalPriceEur(calculatedPrice);
        verify(order).setSubmittedAt(any());
        verify(order).setStatus(OrderStatus.SUBMITTED);
        verify(orderRepository).update(this.order);
        assertEquals(this.order, order);
    }

    @Test(expected = BadRequestException.class)
    public void submitThrowsBadRequestWhenAlreadySubmitted() throws BadRequestException, ServerException {
        when(order.isSubmitted()).thenReturn(true);
        when(order.getStatus()).thenReturn(OrderStatus.SUBMITTED);
        this.service.submitOrder(1);
    }

    @Test(expected = BadRequestException.class)
    public void updateThrowsBadRequestWhenAlreadySubmitted() throws BadRequestException {
        when(order.getStatus()).thenReturn(OrderStatus.SUBMITTED);
        var orderItems = List.of(new OrderItem(product, BigDecimal.ONE));
        this.service.updateOrder(1, 1, orderItems);
    }

    @Test(expected = BadRequestException.class)
    public void updateThrowsBadRequestWhenContainsUnavailableProducts() throws BadRequestException {
        when(productRepository.getProducts(List.of(1L, 2L))).thenReturn(List.of(product, new Product(null, null, null, null, false)));
        var orderItems = List.of(new OrderItem(product, BigDecimal.ONE), new OrderItem(new Product(1L), BigDecimal.ONE));
        this.service.updateOrder(1, 1, orderItems);
    }

    @Test(expected = BadRequestException.class)
    public void updateThrowsBadRequestWhenContainsInexistentProducts() throws BadRequestException {
        when(productRepository.getProducts(List.of(1L, 2L))).thenReturn(List.of(product));
        var orderItems = List.of(new OrderItem(product, BigDecimal.ONE), new OrderItem(new Product(1L), BigDecimal.ONE));
        this.service.updateOrder(1, 1, orderItems);
    }

    @Test(expected = NoSuchElementException.class)
    public void updateThrowsExceptionFromRepository() throws BadRequestException {
        when(productRepository.getProducts(List.of(1L))).thenReturn(List.of(product));
        when(orderRepository.findOrderById(404)).thenThrow(NoSuchElementException.class);
        var orderItems = List.of(new OrderItem(product, BigDecimal.ONE), new OrderItem(new Product(1L), BigDecimal.ONE));
        this.service.updateOrder(404, 1, orderItems);
    }

    @Test(expected = BadRequestException.class)
    public void createThrowsBadRequestWhenContainsUnavailableProducts() throws BadRequestException {
        when(productRepository.getProducts(List.of(1L, 2L))).thenReturn(List.of(product, new Product(null, null, null, null, false)));
        var orderItems = List.of(new OrderItem(product, BigDecimal.ONE), new OrderItem(new Product(1L), BigDecimal.ONE));
        this.service.addOrder( 1, orderItems);
    }

    @Test(expected = BadRequestException.class)
    public void createThrowsBadRequestWhenContainsInexistentProducts() throws BadRequestException {
        when(productRepository.getProducts(List.of(1L, 2L))).thenReturn(List.of(product));
        var orderItems = List.of(new OrderItem(product, BigDecimal.ONE), new OrderItem(new Product(1L), BigDecimal.ONE));
        this.service.addOrder(1, orderItems);
    }
}
