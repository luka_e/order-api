package com.codilium.interview.orderapi;

import com.codilium.interview.domain.spi.CustomerRepository;
import com.codilium.interview.orderapi.domain.model.Customer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class DefaultCustomerServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private Customer customer;

    private DefaultCustomerService service;

    private static final long CUSTOMER_ID = 1;
    private static final String CUSTOMER_FIRST_NAME = "first name";
    private static final String CUSTOMER_LAST_NAME = "last name";
    private static final String CUSTOMER_EMAIL = "email";
    private static final int LIMIT = 50;
    private static final int PAGE = 2;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        this.service = new DefaultCustomerService(customerRepository);
        when(customer.getId()).thenReturn(CUSTOMER_ID);
        when(customerRepository.findById(anyLong())).thenReturn(customer);
        when(customerRepository.findAll(anyInt(), anyInt())).thenReturn(List.of(customer));
        when(customerRepository.update(any(Customer.class))).thenReturn(customer);
        when(customerRepository.create(any(Customer.class))).thenReturn(customer);
        when(customerRepository.delete(anyLong())).thenReturn(customer);
    }

    @Test
    public void callsProductFactoryAndRepositoryCreateMethodOnAddProduct() {
        var returnedCustomer = this.service.addCustomer(CUSTOMER_FIRST_NAME, CUSTOMER_LAST_NAME, CUSTOMER_EMAIL);
        ArgumentCaptor<Customer> captor = ArgumentCaptor.forClass(Customer.class);
        verify(customerRepository).create(captor.capture());
        assertEquals(this.customer, returnedCustomer);
        var capturedCustomer = captor.getValue();
        assertNull(capturedCustomer.getId());
        assertEqualCustomer(capturedCustomer);

    }

    @Test
    public void callsProductFactoryAndRepositoryUpdateMethodOnUpdateProduct() {
        var returnedCustomer = this.service.updateCustomer(CUSTOMER_ID, CUSTOMER_FIRST_NAME, CUSTOMER_LAST_NAME, CUSTOMER_EMAIL);
        ArgumentCaptor<Customer> captor = ArgumentCaptor.forClass(Customer.class);
        verify(customerRepository).update(captor.capture());
        assertEquals(this.customer, returnedCustomer);
        assertEquals(1, (long)customer.getId());
        assertEqualCustomer(captor.getValue());
    }

    private void assertEqualCustomer(Customer customer) {
        assertEquals(CUSTOMER_FIRST_NAME, customer.getFirstName());
        assertEquals(CUSTOMER_LAST_NAME, customer.getLastName());
        assertEquals(CUSTOMER_EMAIL, customer.getEmail());
    }

    @Test
    public void callsValueObjectFactoryAndRepositoryDeleteMethodOnDeleteProduct() {
        var product = this.service.deleteCustomer(CUSTOMER_ID);
        verify(customerRepository).delete(eq(CUSTOMER_ID));
        assertEquals(this.customer, product);
    }

    @Test
    public void callsValueObjectFactoryAndRepositoryGetMethodOnGetProductAndReturnsObject() {
        var product = this.service.getCustomer(CUSTOMER_ID);
        verify(customerRepository).findById(eq(CUSTOMER_ID));
        assertEquals(this.customer, product);
    }

    @Test
    public void getProductsReturnsList() {
        var list = this.service.getCustomers(PAGE, LIMIT);
        assertEquals(List.of(this.customer), list);
    }

    @Test
    public void callsGetProductsWithPassedParametersAnd() {
        this.service.getCustomers(PAGE, LIMIT);
        verify(customerRepository).findAll(eq(PAGE), eq(LIMIT));
    }

    @Test
    public void callsGetProductsWithDefaultParameters() {
        this.service.getCustomers(null, null);
        verify(customerRepository).findAll(eq(1), eq(20));
    }

    @Test
    public void callsGetProductsWithModifiedLimitWhenTooHigh() {
        this.service.getCustomers(null, 200);
        verify(customerRepository).findAll(eq(1), eq(20));
    }

}
