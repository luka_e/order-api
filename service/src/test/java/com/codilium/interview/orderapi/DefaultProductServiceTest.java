package com.codilium.interview.orderapi;

import com.codilium.interview.domain.spi.ProductRepository;
import com.codilium.interview.orderapi.domain.model.Product;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class DefaultProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @Mock
    private Product product;

    private DefaultProductService service;

    private static final long PRODUCT_ID = 1;
    private static final String PRODUCT_NAME = "test product name";
    private static final String PRODUCT_CODE = "1234567890";
    private static final BigDecimal PRODUCT_PRICE = BigDecimal.valueOf(100);
    private static final String PRODUCT_DESCRIPTION = "test product description";
    private static final boolean PRODUCT_AVAILABILITY = true;
    private static final int LIMIT = 50;
    private static final int PAGE = 2;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        this.service = new DefaultProductService(productRepository);
        when(product.getId()).thenReturn(PRODUCT_ID);
        when(productRepository.findById(anyLong())).thenReturn(product);
        when(productRepository.getProducts(anyInt(), anyInt())).thenReturn(List.of(product));
        when(productRepository.update(any(Product.class))).thenReturn(product);
        when(productRepository.create(any(Product.class))).thenReturn(product);
        when(productRepository.delete(anyLong())).thenReturn(product);
    }

    @Test
    public void callsProductFactoryAndRepositoryCreateMethodOnAddProduct() {
        var returnedProduct = this.service.addProduct(PRODUCT_NAME, PRODUCT_CODE, PRODUCT_PRICE, PRODUCT_DESCRIPTION, PRODUCT_AVAILABILITY);
        ArgumentCaptor<Product> captor = ArgumentCaptor.forClass(Product.class);
        verify(productRepository).create(captor.capture());
        assertEquals(this.product, returnedProduct);
        var capturedProduct = captor.getValue();
        assertNull(capturedProduct.getId());
        assertEqualProduct(capturedProduct);

    }

    @Test
    public void callsProductFactoryAndRepositoryUpdateMethodOnUpdateProduct() {
        var returnedProduct = this.service.updateProduct(PRODUCT_ID, PRODUCT_NAME, PRODUCT_CODE, PRODUCT_PRICE, PRODUCT_DESCRIPTION, PRODUCT_AVAILABILITY);
        ArgumentCaptor<Product> captor = ArgumentCaptor.forClass(Product.class);
        verify(productRepository).update(captor.capture());
        assertEquals(this.product, returnedProduct);
        assertEquals(1, (long)product.getId());
        assertEqualProduct(captor.getValue());
    }

    private void assertEqualProduct(Product product) {
        assertEquals(PRODUCT_NAME, product.getName());
        assertEquals(PRODUCT_CODE, product.getCode());
        assertEquals(PRODUCT_PRICE, product.getPrice());
        assertEquals(PRODUCT_DESCRIPTION, product.getDescription());
        assertEquals(PRODUCT_AVAILABILITY, product.getAvailable());
    }

    @Test
    public void callsValueObjectFactoryAndRepositoryDeleteMethodOnDeleteProduct() {
        var product = this.service.deleteProduct(PRODUCT_ID);
        verify(productRepository).delete(eq(PRODUCT_ID));
        assertEquals(this.product, product);
    }

    @Test
    public void callsValueObjectFactoryAndRepositoryGetMethodOnGetProductAndReturnsObject() {
        var product = this.service.getProduct(PRODUCT_ID);
        verify(productRepository).findById(eq(PRODUCT_ID));
        assertEquals(this.product, product);
    }

    @Test
    public void getProductsReturnsList() {
        var list = this.service.getProducts(PAGE, LIMIT);
        assertEquals(List.of(this.product), list);
    }

    @Test
    public void callsGetProductsWithPassedParametersAnd() {
        this.service.getProducts(PAGE, LIMIT);
        verify(productRepository).getProducts(eq(PAGE), eq(LIMIT));
    }

    @Test
    public void callsGetProductsWithDefaultParameters() {
        this.service.getProducts(null, null);
        verify(productRepository).getProducts(eq(1), eq(20));
    }

    @Test
    public void callsGetProductsWithModifiedLimitWhenTooHigh() {
        this.service.getProducts(null, 200);
        verify(productRepository).getProducts(eq(1), eq(20));
    }

}
