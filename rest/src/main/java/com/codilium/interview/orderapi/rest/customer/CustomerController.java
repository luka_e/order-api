package com.codilium.interview.orderapi.rest.customer;

import com.codilium.interview.domain.api.CustomerService;
import com.codilium.interview.orderapi.domain.model.Customer;
import com.codilium.interview.orderapi.rest.customer.model.CustomerRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping("/{id}")
    public ResponseEntity<Customer> getCustomer(
            @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(customerService.getCustomer(id));
    }

    @GetMapping
    public ResponseEntity<List<Customer>> getCustomers(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer limit
    ) {
        return ResponseEntity.ok(customerService.getCustomers(page, limit));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Customer> deleteCustomer(
            @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(customerService.deleteCustomer(id));
    }

    @PostMapping
    public ResponseEntity<Customer> createCustomer(
            @Valid @RequestBody CustomerRequest customerRequest
    ) {
        return ResponseEntity.ok(
                customerService.addCustomer(
                        customerRequest.getFirstName(),
                        customerRequest.getLastName(),
                        customerRequest.getEmail()));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Customer> updateCustomer(
            @PathVariable("id") Long id,
            @Valid @RequestBody CustomerRequest customerRequest
    ) {
        return ResponseEntity.ok(
                customerService.updateCustomer(
                        id,
                        customerRequest.getFirstName(),
                        customerRequest.getLastName(),
                        customerRequest.getEmail()));
    }
}
