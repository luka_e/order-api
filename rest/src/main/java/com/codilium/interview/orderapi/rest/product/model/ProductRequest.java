package com.codilium.interview.orderapi.rest.product.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest {

    @NotBlank(message = "Product name must be specified")
    private String name;

    @NotNull(message = "Product code must be specified")
    @Size(min = 10, max = 10, message = "Product code must have a length of 10")
    private String code;

    @Min(value = 0, message = "Product price cannot be less than zero")
    private BigDecimal price;

    private String description;

    @NotNull(message = "Product availability must be specified")
    private Boolean isAvailable;
}
