package com.codilium.interview.orderapi.rest.order.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderRequest {

    @NotNull(message = "Customer ID must be specified")
    private Long customerId;

    private List<OrderItemRequest> orderItems;


}
