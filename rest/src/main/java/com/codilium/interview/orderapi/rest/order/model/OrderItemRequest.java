package com.codilium.interview.orderapi.rest.order.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemRequest {

    @NotNull(message = "Product ID in order item must be specified")
    private Long productId;

    @NotNull(message = "Quantity in order item must be specified")
    private BigDecimal quantity;
}
