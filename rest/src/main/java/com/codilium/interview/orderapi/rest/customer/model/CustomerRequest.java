package com.codilium.interview.orderapi.rest.customer.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRequest {

    @NotBlank(message = "Customer first name must be specified")
    private String firstName;

    @NotBlank(message = "Customer last name must be specified")
    private String lastName;

    @NotBlank(message = "Customer email must be specified")
    @Email(message = "Customer email not valid")
    private String email;

}
