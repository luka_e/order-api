package com.codilium.interview.orderapi.rest.product;

import com.codilium.interview.domain.api.ProductService;
import com.codilium.interview.orderapi.domain.model.Product;
import com.codilium.interview.orderapi.rest.product.model.ProductRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProduct(
            @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(productService.getProduct(id));
    }

    @GetMapping
    public ResponseEntity<List<Product>> getProducts(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer limit
    ) {
        return ResponseEntity.ok(productService.getProducts(page, limit));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Product> deleteProduct(
            @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(productService.deleteProduct(id));
    }

    @PostMapping
    public ResponseEntity<Product> createProduct(
            @Valid @RequestBody ProductRequest productRequest
    ) {
        return ResponseEntity.ok(
                productService.addProduct(
                        productRequest.getName(),
                        productRequest.getCode(),
                        productRequest.getPrice(),
                        productRequest.getDescription(),
                        productRequest.getIsAvailable()));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> updateProduct(
            @PathVariable("id") Long id,
            @Valid @RequestBody ProductRequest productRequest
    ) {
        return ResponseEntity.ok(
                productService.updateProduct(
                        id,
                        productRequest.getName(),
                        productRequest.getCode(),
                        productRequest.getPrice(),
                        productRequest.getDescription(),
                        productRequest.getIsAvailable()));
    }
}
