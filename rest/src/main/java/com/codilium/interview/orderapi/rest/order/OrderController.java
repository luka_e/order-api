package com.codilium.interview.orderapi.rest.order;

import com.codilium.interview.domain.api.OrderService;
import com.codilium.interview.domain.api.exception.BadRequestException;
import com.codilium.interview.domain.api.exception.ServerException;
import com.codilium.interview.orderapi.domain.model.Order;
import com.codilium.interview.orderapi.domain.model.OrderItem;
import com.codilium.interview.orderapi.domain.model.Product;
import com.codilium.interview.orderapi.rest.order.model.OrderItemRequest;
import com.codilium.interview.orderapi.rest.order.model.OrderRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @GetMapping("/{id}")
    public ResponseEntity<Order> getOrder(
            @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(orderService.getOrder(id));
    }

    @GetMapping
    public ResponseEntity<List<Order>> getOrders(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer limit
    ) {
        return ResponseEntity.ok(orderService.getOrders(page, limit));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Order> deleteOrder(
            @PathVariable("id") Long id
    ) throws BadRequestException {
        return ResponseEntity.ok(orderService.deleteOrder(id));
    }

    @PostMapping
    public ResponseEntity<Order> createOrder(
            @Valid @RequestBody OrderRequest orderRequest
    ) throws BadRequestException {
        return ResponseEntity.ok(
                orderService.addOrder(
                        orderRequest.getCustomerId(),
                        toOrderItems(orderRequest.getOrderItems())));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Order> updateOrder(
            @PathVariable("id") Long id,
            @Valid @RequestBody OrderRequest orderRequest
    ) throws BadRequestException {
        return ResponseEntity.ok(
                orderService.updateOrder(
                        id,
                        orderRequest.getCustomerId(),
                        toOrderItems(orderRequest.getOrderItems())));
    }

    @PatchMapping("/{id}/submit")
    public ResponseEntity<Order> submitOrder(
            @PathVariable("id") long id
    ) throws BadRequestException, ServerException {
        return ResponseEntity.ok(orderService.submitOrder(id));
    }

    private List<OrderItem> toOrderItems(List<OrderItemRequest> orderItemRequests) {
        return orderItemRequests
                .stream()
                .map( orderItemRequest ->
                        new OrderItem(
                                new Product(orderItemRequest.getProductId()),
                                orderItemRequest.getQuantity()
                        )
                )
                .collect(Collectors.toList());
    }
}
