package com.codilium.interview.domain.api.exception;

public class ServerException extends Exception {

    public ServerException(Throwable cause) {
        super(cause);
    }

    public ServerException(String message) {
        super(message);
    }

    public ServerException() {
        super("Something went wrong");
    }
}
