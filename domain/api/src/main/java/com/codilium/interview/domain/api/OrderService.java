package com.codilium.interview.domain.api;

import com.codilium.interview.domain.api.exception.BadRequestException;
import com.codilium.interview.domain.api.exception.ServerException;
import com.codilium.interview.orderapi.domain.model.Order;
import com.codilium.interview.orderapi.domain.model.OrderItem;

import java.util.List;

public interface OrderService {
    Order getOrder(long id);
    Order addOrder(long customerId, List<OrderItem> orderItems) throws BadRequestException;
    Order updateOrder(long orderId, long customerId, List<OrderItem> orderItems) throws BadRequestException;
    Order deleteOrder(long id) throws BadRequestException;
    List<Order> getOrders(Integer page, Integer limit);
    Order submitOrder(long orderId) throws BadRequestException, ServerException;
}
