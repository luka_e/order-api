package com.codilium.interview.domain.api;

import com.codilium.interview.orderapi.domain.model.Customer;

import java.util.List;

public interface CustomerService {
    Customer addCustomer(String firstName, String lastName, String email);
    Customer updateCustomer(long id, String firstName, String lastName, String email);
    Customer deleteCustomer(long id);
    Customer getCustomer(long id);
    List<Customer> getCustomers(Integer page, Integer limit);
}
