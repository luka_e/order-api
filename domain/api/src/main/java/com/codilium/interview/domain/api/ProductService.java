package com.codilium.interview.domain.api;

import com.codilium.interview.orderapi.domain.model.Product;

import java.math.BigDecimal;
import java.util.List;

public interface ProductService {
    Product addProduct(String name, String code, BigDecimal price, String description, Boolean isAvailable);
    Product updateProduct(Long id, String name, String code, BigDecimal price, String description, Boolean isAvailable);
    Product deleteProduct(Long id);
    Product getProduct(Long id);
    List<Product> getProducts(Integer page, Integer limit);
}
