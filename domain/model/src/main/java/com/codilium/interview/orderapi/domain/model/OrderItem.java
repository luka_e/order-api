package com.codilium.interview.orderapi.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItem {
    private Long id;
    private Product product;
    private BigDecimal quantity;

    public OrderItem(Product product, BigDecimal quantity) {
        this.product = product;
        this.quantity = quantity;
    }
}
