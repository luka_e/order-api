package com.codilium.interview.orderapi.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private Long id;
    private Customer customer;
    private OrderStatus status = OrderStatus.DRAFT;
    private BigDecimal totalPriceHrk;
    private BigDecimal totalPriceEur;
    private Date submittedAt;
    private List<OrderItem> orderItems;


    public Order(Customer customer, List<OrderItem> orderItems, BigDecimal totalPrice) {
        this.customer = customer;
        this.orderItems = orderItems;
        this.totalPriceHrk = totalPrice;
    }

    public boolean isSubmitted() {
        return status == OrderStatus.SUBMITTED;
    }
}
