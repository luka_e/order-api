package com.codilium.interview.orderapi.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    private Long id;
    private String name;
    private String code;
    private BigDecimal price;
    private String description;
    private Boolean available;

    public Product(String name, String code, BigDecimal price, String description, Boolean available) {
        this.name = name;
        this.code = code;
        this.price = price;
        this.description = description;
        this.available = available;
    }

    public Product(Long productId) {
        this.id = productId;
    }
}
