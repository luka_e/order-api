package com.codilium.interview.orderapi.domain.model;

public enum OrderStatus {
    DRAFT, SUBMITTED
}
