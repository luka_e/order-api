package com.codilium.interview.domain.spi;

import com.codilium.interview.orderapi.domain.model.Customer;

import java.util.List;

public interface CustomerRepository {
    Customer create(Customer customer);
    Customer update(Customer customer);
    Customer delete(long id);
    Customer findById(long id);
    List<Customer> findAll(int page, int limit);
}
