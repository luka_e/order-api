package com.codilium.interview.domain.spi;

import com.codilium.interview.orderapi.domain.model.Order;

import java.util.List;

public interface OrderRepository {
    Order findOrderById(long id);

    List<Order> getOrders(Integer page, Integer limit);

    Order create(Order order);

    Order update(Order order);

    void delete(long id);
}
