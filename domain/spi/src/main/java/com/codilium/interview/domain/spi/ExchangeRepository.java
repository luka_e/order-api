package com.codilium.interview.domain.spi;

import com.codilium.interview.domain.api.exception.ServerException;

import java.math.BigDecimal;

public interface ExchangeRepository {
    BigDecimal getExchangeRate() throws ServerException;
}
