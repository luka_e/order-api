package com.codilium.interview.domain.spi;

import com.codilium.interview.orderapi.domain.model.Product;

import java.util.List;

public interface ProductRepository {
    Product findById(long id);

    List<Product> getProducts(Integer page, Integer limit);

    Product create(Product product);

    Product update(Product product);

    Product delete(long id);

    List<Product> getProducts(List<Long> ids);
}
