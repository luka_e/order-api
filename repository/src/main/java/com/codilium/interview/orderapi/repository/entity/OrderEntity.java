package com.codilium.interview.orderapi.repository.entity;

import com.codilium.interview.orderapi.domain.model.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "WEBSHOP_ORDER")
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    CustomerEntity customer;

    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    private BigDecimal totalPriceHrk;
    private BigDecimal totalPriceEur;

    private Date submittedAt;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "ORDER_ID")
    private List<OrderItemEntity> orderItems;
}
