package com.codilium.interview.orderapi.repository.mapper;

import com.codilium.interview.orderapi.domain.model.OrderItem;
import com.codilium.interview.orderapi.domain.model.Product;
import com.codilium.interview.orderapi.repository.entity.OrderItemEntity;
import com.codilium.interview.orderapi.repository.entity.ProductEntity;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class OrderItemMapper implements Mapper<OrderItemEntity, OrderItem> {

    private final Mapper<ProductEntity, Product> productMapper;

    @Override
    public OrderItemEntity toEntity(OrderItem aggregate) {
        return new OrderItemEntity(
                productMapper.toEntity(aggregate.getProduct()),
                aggregate.getQuantity());
    }

    @Override
    public OrderItem toAggregate(OrderItemEntity entity) {
        return new OrderItem(
                entity.getId(),
                productMapper.toAggregate(entity.getProduct()),
                entity.getQuantity());
    }
}
