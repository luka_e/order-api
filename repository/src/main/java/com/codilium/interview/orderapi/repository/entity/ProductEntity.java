package com.codilium.interview.orderapi.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "PRODUCT")
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String code;

    private BigDecimal price;

    private String description;

    private Boolean isAvailable;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
    private List<OrderItemEntity> orderItems;

    public ProductEntity(Long id) {
        this.id = id;
    }

    public ProductEntity(Long id, String name, String code, BigDecimal price, String description, Boolean isAvailable) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.price = price;
        this.description = description;
        this.isAvailable = isAvailable;
    }
}
