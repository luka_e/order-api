package com.codilium.interview.orderapi.repository.mapper;

import com.codilium.interview.orderapi.domain.model.Customer;
import com.codilium.interview.orderapi.domain.model.Order;
import com.codilium.interview.orderapi.domain.model.OrderItem;
import com.codilium.interview.orderapi.repository.entity.CustomerEntity;
import com.codilium.interview.orderapi.repository.entity.OrderEntity;
import com.codilium.interview.orderapi.repository.entity.OrderItemEntity;
import lombok.RequiredArgsConstructor;

import java.util.stream.Collectors;

@RequiredArgsConstructor
public class OrderMapper implements Mapper<OrderEntity, Order> {

    private final Mapper<CustomerEntity, Customer> customerMapper;
    private final Mapper<OrderItemEntity, OrderItem> orderItemMapper;

    @Override
    public OrderEntity toEntity(Order order) {
        return new OrderEntity(
                order.getId(),
                customerMapper.toEntity(order.getCustomer()),
                order.getStatus(),
                order.getTotalPriceHrk(),
                order.getTotalPriceEur(),
                order.getSubmittedAt(),
                order.getOrderItems().stream().map(orderItemMapper::toEntity).collect(Collectors.toList())
        );
    }

    @Override
    public Order toAggregate(OrderEntity entity) {
        return new Order(
                entity.getId(),
                customerMapper.toAggregate(entity.getCustomer()),
                entity.getStatus(),
                entity.getTotalPriceHrk(),
                entity.getTotalPriceEur(),
                entity.getSubmittedAt(),
                entity.getOrderItems().stream().map(orderItemMapper::toAggregate).collect(Collectors.toList())
        );
    }
}
