package com.codilium.interview.orderapi.repository.product;

import com.codilium.interview.orderapi.repository.entity.ProductEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends CrudRepository<ProductEntity, Long> {
    Optional<ProductEntity> findById(long id);
    //Optional<ProductEntity> save(ProductEntity productEntity);
    Optional<ProductEntity> deleteById(long id);
    List<ProductEntity> findAll(Pageable pageable);

}
