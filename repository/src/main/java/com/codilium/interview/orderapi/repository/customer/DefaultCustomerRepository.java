package com.codilium.interview.orderapi.repository.customer;

import com.codilium.interview.domain.spi.CustomerRepository;
import com.codilium.interview.orderapi.domain.model.Customer;
import com.codilium.interview.orderapi.repository.entity.CustomerEntity;
import com.codilium.interview.orderapi.repository.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class DefaultCustomerRepository implements CustomerRepository {

    private final com.codilium.interview.orderapi.repository.customer.CustomerRepository jpaCustomerRepository;
    private final Mapper<CustomerEntity, Customer> mapper;

    @Override
    public Customer create(Customer customer) {
        return mapper.toAggregate(jpaCustomerRepository.save(mapper.toEntity(customer)));
    }

    @Override
    public Customer update(Customer customer) {
        return mapper.toAggregate(jpaCustomerRepository.save(mapper.toEntity(customer)));
    }

    @Override
    public Customer delete(long id) {
        val customerEntity = jpaCustomerRepository.findById(id).orElseThrow();
        jpaCustomerRepository.deleteById(id);
        return mapper.toAggregate(customerEntity);
    }

    @Override
    public Customer findById(long id) {
        return mapper.toAggregate(jpaCustomerRepository.findById(id).orElseThrow());
    }

    @Override
    public List<Customer> findAll(int page, int limit) {
        return jpaCustomerRepository.findAll(PageRequest.of(page - 1, limit))
                .stream()
                .map(mapper::toAggregate)
                .collect(Collectors.toList());
    }
}
