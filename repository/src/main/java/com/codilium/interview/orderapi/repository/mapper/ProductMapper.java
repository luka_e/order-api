package com.codilium.interview.orderapi.repository.mapper;

import com.codilium.interview.orderapi.domain.model.Product;
import com.codilium.interview.orderapi.repository.entity.ProductEntity;

public class ProductMapper implements Mapper<ProductEntity, Product> {

    @Override
    public Product toAggregate(ProductEntity productEntity) {
        return new Product(
                productEntity.getId(),
                productEntity.getName(),
                productEntity.getCode(),
                productEntity.getPrice(),
                productEntity.getDescription(),
                productEntity.getIsAvailable()
        );
    }

    @Override
    public ProductEntity toEntity(Product product) {
        return new ProductEntity(
                product.getId(),
                product.getName(),
                product.getCode(),
                product.getPrice(),
                product.getDescription(),
                product.getAvailable()
        );
    }
}
