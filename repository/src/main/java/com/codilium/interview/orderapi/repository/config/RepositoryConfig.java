package com.codilium.interview.orderapi.repository.config;

import com.codilium.interview.domain.spi.CustomerRepository;
import com.codilium.interview.domain.spi.OrderRepository;
import com.codilium.interview.domain.spi.ProductRepository;
import com.codilium.interview.orderapi.domain.model.Customer;
import com.codilium.interview.orderapi.domain.model.Order;
import com.codilium.interview.orderapi.domain.model.Product;
import com.codilium.interview.orderapi.repository.customer.DefaultCustomerRepository;
import com.codilium.interview.orderapi.repository.entity.CustomerEntity;
import com.codilium.interview.orderapi.repository.entity.OrderEntity;
import com.codilium.interview.orderapi.repository.entity.ProductEntity;
import com.codilium.interview.orderapi.repository.mapper.Mapper;
import com.codilium.interview.orderapi.repository.order.DefaultOrderRepository;
import com.codilium.interview.orderapi.repository.product.DefaultProductRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(MapperConfig.class)
public class RepositoryConfig {

    @Bean
    public ProductRepository provideProductRepository(
            com.codilium.interview.orderapi.repository.product.ProductRepository jpaProductRepository,
            Mapper<ProductEntity, Product> mapper
    ) {
        return new DefaultProductRepository(jpaProductRepository, mapper);
    }

    @Bean
    public CustomerRepository provideCustomerRepository(
            com.codilium.interview.orderapi.repository.customer.CustomerRepository jpaCustomerRepository,
            Mapper<CustomerEntity, Customer> mapper
    ) {
        return new DefaultCustomerRepository(jpaCustomerRepository, mapper);
    }

    @Bean
    public OrderRepository provideOrderRepository(
            com.codilium.interview.orderapi.repository.order.OrderRepository jpaOrderRepository,
            Mapper<OrderEntity, Order> mapper
    ) {
        return new DefaultOrderRepository(jpaOrderRepository, mapper);
    }
}
