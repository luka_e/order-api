package com.codilium.interview.orderapi.repository.config;

import com.codilium.interview.orderapi.domain.model.Customer;
import com.codilium.interview.orderapi.domain.model.Order;
import com.codilium.interview.orderapi.domain.model.OrderItem;
import com.codilium.interview.orderapi.domain.model.Product;
import com.codilium.interview.orderapi.repository.entity.CustomerEntity;
import com.codilium.interview.orderapi.repository.entity.OrderEntity;
import com.codilium.interview.orderapi.repository.entity.OrderItemEntity;
import com.codilium.interview.orderapi.repository.entity.ProductEntity;
import com.codilium.interview.orderapi.repository.mapper.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {

    @Bean
    public Mapper<ProductEntity, Product> provideProductMapper() {
        return new ProductMapper();
    }

    @Bean
    public Mapper<CustomerEntity, Customer> provideCustomerMapper() {
        return new CustomerMapper();
    }

    @Bean
    public Mapper<OrderEntity, Order> provideOrderMapper(
            Mapper<CustomerEntity, Customer> customerMapper,
            Mapper<OrderItemEntity, OrderItem> orderItemMapper
    ) {
        return new OrderMapper(customerMapper, orderItemMapper);
    }

    @Bean
    public Mapper<OrderItemEntity, OrderItem> provideOrderItemMapper(Mapper<ProductEntity, Product> productMapper) {
        return new OrderItemMapper(productMapper);
    }
}
