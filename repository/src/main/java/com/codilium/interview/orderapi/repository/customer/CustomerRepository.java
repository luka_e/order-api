package com.codilium.interview.orderapi.repository.customer;

import com.codilium.interview.orderapi.repository.entity.CustomerEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends CrudRepository<CustomerEntity, Long> {
    List<CustomerEntity> findAll(Pageable pageable);
}
