package com.codilium.interview.orderapi.repository.mapper;

public interface Mapper<T, R> {
    T toEntity(R aggregate);
    R toAggregate(T entity);
}
