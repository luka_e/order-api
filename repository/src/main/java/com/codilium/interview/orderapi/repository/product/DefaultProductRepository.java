package com.codilium.interview.orderapi.repository.product;

import com.codilium.interview.orderapi.domain.model.Product;
import com.codilium.interview.orderapi.repository.entity.ProductEntity;
import com.codilium.interview.orderapi.repository.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class DefaultProductRepository implements com.codilium.interview.domain.spi.ProductRepository {

    private final ProductRepository jpaProductRepository;
    private final Mapper<ProductEntity, Product> mapper;

    @Override
    public Product findById(long id) {
        return mapper.toAggregate(jpaProductRepository.findById(id).orElseThrow());
    }

    @Override
    public List<Product> getProducts(Integer page, Integer limit) {
        return jpaProductRepository
                .findAll(PageRequest.of(page - 1, limit))
                .stream()
                .map(mapper::toAggregate)
                .collect(Collectors.toList());
    }

    @Override
    public Product create(Product product) {
        return mapper.toAggregate(jpaProductRepository.save(mapper.toEntity(product)));
    }

    @Override
    public Product update(Product product) {
        return mapper.toAggregate(jpaProductRepository.save(mapper.toEntity(product)));
    }

    @Override
    public Product delete(long id) {
        val product = jpaProductRepository.findById(id).orElseThrow();
        jpaProductRepository.deleteById(id);
        return mapper.toAggregate(product);
    }

    @Override
    public List<Product> getProducts(List<Long> ids) {
        val products = new ArrayList<Product>();
        jpaProductRepository.findAllById(ids).forEach(productEntity -> products.add(mapper.toAggregate(productEntity)));
        return products;
    }
}
