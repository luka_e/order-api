package com.codilium.interview.orderapi.repository.order;

import com.codilium.interview.orderapi.repository.entity.OrderEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<OrderEntity, Long> {
    List<OrderEntity> findAll(Pageable pageable);
}
