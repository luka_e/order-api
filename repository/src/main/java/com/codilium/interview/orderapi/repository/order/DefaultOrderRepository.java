package com.codilium.interview.orderapi.repository.order;

import com.codilium.interview.domain.spi.OrderRepository;
import com.codilium.interview.orderapi.domain.model.Order;
import com.codilium.interview.orderapi.repository.entity.OrderEntity;
import com.codilium.interview.orderapi.repository.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class DefaultOrderRepository implements OrderRepository {

    private final com.codilium.interview.orderapi.repository.order.OrderRepository jpaOrderRepository;
    private final Mapper<OrderEntity, Order> mapper;

    @Override
    public Order findOrderById(long id) {
        return mapper.toAggregate(this.jpaOrderRepository.findById(id).orElseThrow());
    }

    @Override
    public List<Order> getOrders(Integer page, Integer limit) {
        return jpaOrderRepository
                .findAll(PageRequest.of(page - 1, limit))
                .stream()
                .map(mapper::toAggregate)
                .collect(Collectors.toList());
    }

    @Override
    public Order create(Order order) {
        val orderEntity = jpaOrderRepository.save(mapper.toEntity(order));
        orderEntity.getOrderItems().forEach(orderItemEntity -> orderItemEntity.setOrder(orderEntity));
        return mapper.toAggregate(orderEntity);
    }

    @Override
    public Order update(Order order) {
        val orderEntity = jpaOrderRepository.save(mapper.toEntity(order));
        orderEntity.getOrderItems().forEach(orderItemEntity -> orderItemEntity.setOrder(orderEntity));
        return mapper.toAggregate(orderEntity);
    }

    @Override
    public void delete(long id) {
        jpaOrderRepository.deleteById(id);
    }
}
