package com.codilium.interview.orderapi.repository.mapper;

import com.codilium.interview.orderapi.domain.model.Customer;
import com.codilium.interview.orderapi.repository.entity.CustomerEntity;

public class CustomerMapper implements Mapper<CustomerEntity, Customer> {

    @Override
    public CustomerEntity toEntity(Customer customer) {
        return new CustomerEntity(
                customer.getId(),
                customer.getFirstName(),
                customer.getLastName(),
                customer.getEmail()
        );
    }

    @Override
    public Customer toAggregate(CustomerEntity customerEntity) {
        return new Customer(
                customerEntity.getId(),
                customerEntity.getFirstName(),
                customerEntity.getLastName(),
                customerEntity.getEmail()
        );
    }
}
