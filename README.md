
### Setup ###

To run the app, follow these steps:

* Create environment variables:
    * ORDER_API_DB_USER
    * ORDER_API_DB_PASSWORD
    * ORDER_API_DB_URL
* docker-compose up
* mvnw -pl app -am spring-boot:run

Tests don't reqire any setup, they work with an embedded database


Swagger endpoints:
* http://localhost:8080/api/v3/api-docs for API docs JSON
* http://localhost:8080/api/swagger-ui.html for Swagger UI 